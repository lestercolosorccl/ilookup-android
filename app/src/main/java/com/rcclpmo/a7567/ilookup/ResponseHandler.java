package com.rcclpmo.a7567.ilookup;

/**
 * Created by 7567 on 21/05/2018.
 */

public interface ResponseHandler {

    public void onSuccess(int requestCode, Object object);
    public void onFailed(int requestCode, String message);

}