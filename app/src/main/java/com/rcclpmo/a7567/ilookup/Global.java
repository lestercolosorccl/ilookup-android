package com.rcclpmo.a7567.ilookup;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import android.app.Application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by 7567 on 06/02/2018.
 */

public class Global extends Application{
    public static String ServerConn = "https://dev.rcclpmo.com/";
    private Context context;
    private static Global instance;

    public static Global getInstance() {
        return instance;
    }


    public Global(Context cntxt) {
        context = cntxt;
    }

    public boolean isOnline() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else {
            connected = false;
//            Toast.makeText(context, "No Internet connection!", Toast.LENGTH_LONG).show();
        }

        return connected;
    }

    public void Alert(String contents){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(contents);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }


   public void Toast(String contents){
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, contents, duration);
        toast.show();
    }


    public String ConvertToDate(String date,String pattern){
        SimpleDateFormat sdf = new SimpleDateFormat(pattern); // here set the pattern as you date in string was containing like date/month/year
        String d = null;
        try {
            d = String.valueOf(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }


    public String parseDateToddMMyyyy(String time, String inputPattern, String outputPattern) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void setMenu(){



    }

}
