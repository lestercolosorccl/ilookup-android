package com.rcclpmo.a7567.ilookup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


import com.android.volley.VolleyError;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.rcclpmo.a7567.ilookup.MainActivity.month;
import static com.rcclpmo.a7567.ilookup.MainActivity.year;
import static com.rcclpmo.a7567.ilookup.MainActivity.shipcode;
import static com.rcclpmo.a7567.ilookup.MainActivity.shipname;
import static java.lang.String.*;

import java.util.GregorianCalendar;

public class CalendarActivity extends AppCompatActivity implements OnDateSelectedListener, OnMonthChangedListener {
    private Global global;

    private  ProgressDialog progressBar;

    private Calendar currentDate;
    private MaterialCalendarView calendarView;

    private Date date = new Date();
    Calendar cal = Calendar.getInstance();
    private static final DateFormat FORMATTER = SimpleDateFormat.getDateInstance();
    private JSONArray lastyearsail;

    private Spinner YearOption;
    private Spinner MonthOption;

    public int selectedDateKey = 0;
    public String selectedDate;

    ArrayList<String> saildates;
    ArrayList<String> yearOptionArray = new ArrayList<String>();
    ArrayList<String> monthOptionArray = new ArrayList<String>();

    TextView textYear;
    TextView textMonth;

    boolean initSpinner = false;
    String defaultdate;
    String defaultdateselected;

    String selectedsaildate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        //close the activity
        ImageView closeButton = (ImageView) findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        LoaderInit();

        global = new Global(this);
        calendarView = (MaterialCalendarView) findViewById(R.id.calendarView);
        calendarView.setSelectionColor(getResources().getColor(R.color.CalendarBg));

        calendarView.setOnDateChangedListener(this);
        calendarView.setOnMonthChangedListener(this);

        YearOption = (Spinner) findViewById(R.id.yearOption);
        MonthOption = (Spinner) findViewById(R.id.monthOption);

        TextView textSelectYear = (TextView) findViewById(R.id.textSelectYear);
        TextView textSelectMonth = (TextView) findViewById(R.id.textSelectMonth);

        textYear = (TextView) findViewById(R.id.textYear);
        textMonth = (TextView) findViewById(R.id.textMonth);

        //activate year option spinner
        textSelectYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YearOption.performClick();
            }
        });
        //activate month option spinner
        textSelectMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MonthOption.performClick();
            }
        });

        defaultdate = getIntent().getExtras().getString("defaultdate");
        defaultdateselected = getIntent().getExtras().getString("defaultdateselected");

        SetTitle();
        initializeCalendar();

        PopulateSelection();
        if(defaultdate!=null){
            String [] sdateParts= defaultdate.split("/");
            Integer dmonth    = Integer.valueOf(sdateParts[0]);
            Integer dyear  = Integer.valueOf(sdateParts[2]);
            month = dmonth;
            year = dyear;
            calendarView.setCurrentDate(CalendarDay.from(dyear, dmonth-1, 1));
            textYear.setText(String.valueOf(dyear));
            textMonth.setText(String.valueOf(getMonth(dmonth-1)));
        }else{
            Date d = Calendar.getInstance().getTime();
            SimpleDateFormat m = new SimpleDateFormat("MM");
            month = Integer.valueOf(m.format(d));
            SimpleDateFormat y = new SimpleDateFormat("yyyy");
            year = Integer.valueOf(y.format(d));
        }

        GetSailDate("sail_date");
    }



    private void initializeCalendar() {;
        ArrayList<CalendarDay> List = new ArrayList<>();
        List.add(CalendarDay.today());

        calendarView.addDecorator(new DateDecorator(getActivity(),  R.color.colorWhite,"current_date", List));
        //set min max of calendar
        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat y = new SimpleDateFormat("yyyy");
        Integer yr = Integer.valueOf(y.format(d));

        calendarView.state().edit()
                .setMinimumDate(CalendarDay.from(yr, 0, 1))
                .setMaximumDate(CalendarDay.from(yr+4, 11, 31))
                .commit();

    }

    private void setSaildates(JSONArray sailList, String type) throws JSONException {

        ArrayList<CalendarDay> List = new ArrayList<>();
        saildates = new ArrayList<>();

        int i;
        if(sailList!=null){
            for(i=0; i<sailList.length(); i++){
                JSONObject saildate = new JSONObject(sailList.getString(i));
                String sdate = saildate.getString("saildate");
                String [] sdateParts = sdate.split("/");
                Integer dayparts = Integer.valueOf(sdateParts[1]);
                Integer monthparts = Integer.valueOf(sdateParts[0]);
                Integer yearparts = Integer.valueOf(sdateParts[2]);
                List.add(CalendarDay.from(yearparts, monthparts-1, dayparts));
                saildates.add(monthparts+"/"+dayparts+"/"+yearparts);
            }
            calendarView.addDecorator(new DateDecorator(getActivity(),  R.color.colorWhite,type, List));
            setSelectedAroundDate();
        }
    }

    private void setSelectedAroundDate(){

        if(defaultdate!=null){
            ArrayList<CalendarDay> List = new ArrayList<>();
            String [] sdateParts= defaultdate.split("/");
//            String [] sdateParts= defaultdateselected.split("/");
            Integer dmonth    = Integer.valueOf(sdateParts[0]);
            Integer dyear  = Integer.valueOf(sdateParts[2]);
            Integer dday  = Integer.valueOf(sdateParts[1]);

            List.add(CalendarDay.from(dyear, dmonth-1, dday));
            calendarView.addDecorator(new DateDecorator(getActivity(),  R.color.colorWhite,"selected_date", List));
        }
    }

    private void GetSailDate(final String type){


        try {

            progressBar.setMessage("Retrieving Sail Dates ...");
            progressBar.show();

            Api.SailDates(shipcode, year, month, new VolleyCallback() {
                @Override
                public void onSuccessResponse(String result) {
                    try {
                        JSONObject response = new JSONObject(result);
                        if(response.getBoolean("success")){

                            JSONArray data = response.getJSONArray("data");
//                            data.length();

                            setSaildates(data, type);
                            lastyearsail = data;
                        }
                        progressBar.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressBar.dismiss();
                        global.Toast("Error!");
                    }
                }

                @Override
                public void onFailedResponse(VolleyError result) {
                    progressBar.dismiss();
                    global.Toast("Error!");
                }
            });
        } catch (JSONException e) {
            global.Toast("Error!");
        }


    }

    private void PopulateSelection(){

        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat y = new SimpleDateFormat("yyyy");
        Integer yr = Integer.valueOf(y.format(d));


        Integer i = yr;
        Integer ypos = 0;
        for(i=yr; i<yr+5; i++){
            yearOptionArray.add(valueOf(i));
            if(i < year ){
                ypos++;
            }
        }

        Integer ii;
        Integer mpos = 0;
        for (ii=0; ii<12; ii++){
            monthOptionArray.add(valueOf(getMonth(ii)));
            if(ii==Integer.valueOf(month-1)){
                mpos = ii;
            }
        }

        ArrayAdapter<String> yearadapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, yearOptionArray);
        YearOption.setAdapter(yearadapter);
        YearOption.setSelection(year);

        ArrayAdapter<String> monthadapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, monthOptionArray);
        MonthOption.setAdapter(monthadapter);

        YearOption.setSelected(false);
        YearOption.setSelection(ypos,true);
        YearOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                ChangeYear(Integer.valueOf(yearOptionArray.get(position)));
            }
            public void onNothingSelected(AdapterView<?> parent)
            {
                global.Toast("nothing selected.");
            }
        });

        MonthOption.setSelected(false);
        MonthOption.setSelection(mpos,true);
        MonthOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                ChangeMonth(position);
            }
            public void onNothingSelected(AdapterView<?> parent)
            {
                global.Toast("nothing selected.");
            }
        });


        initSpinner = true;
    }


    public void SelectYear(Integer y){
        Integer pos = 0;
        Integer i;
        for(i=0; i<yearOptionArray.size(); i++){
            if(yearOptionArray.get(i).equals(y.toString())){
                pos = i;
            }
        }
        YearOption.setSelection(pos);
    }

    public void SelectMonth(Integer m){
        Integer pos = m;
        MonthOption.setSelection(pos);
    }


    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
        if(initSpinner){
            ChangeMonth(date.getMonth());
            SelectMonth(date.getMonth());
            if(year!=date.getYear()) {
//                ChangeYear(date.getYear());
                SelectYear(date.getYear());
            }
        }
    }


    public void LoaderInit(){
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setCanceledOnTouchOutside(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setMax(100);
        progressBar.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_BACK && !event.isCanceled()) {
                    if(progressBar.isShowing()) {
                        //your logic here for back button pressed event
                        Api.CancelCall();
                        progressBar.dismiss();
                    }
                    return true;
                }
                return false;
            }
        });
    }


    public void ChangeMonth(Integer m){
        month = m;
        textMonth.setText(valueOf(getMonth(month)));
        calendarView.setCurrentDate(CalendarDay.from(year, month, 1));
    }

    public void ChangeYear(Integer y){
            year = y;
            textYear.setText(valueOf(year));
            calendarView.setCurrentDate(CalendarDay.from(year, month, 1));
            try {
                setSaildates(lastyearsail, "remove_saildate");
                GetSailDate("sail_date");
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    public String getMonth(int m) {
        DateFormat formatter = new SimpleDateFormat("MMM");
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, m);
        return formatter.format(calendar.getTime());
    }


    private Context getActivity() {
        return this;
    }


    private void SetTitle() {
        TextView ShipName = (TextView) findViewById(R.id.toolbar_title);
        ShipName.setText(shipname + " (" + shipcode + ")");
    }


    private String getSelectedDatesString() {
        CalendarDay date = calendarView.getSelectedDate();
        if (date == null) {
            return "No Selection";
        }
        return valueOf(date.getDate());
    }


    public void onBackPressed(){
        Intent intent = new Intent();
        setResult(0, intent);
        finish();
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @Nullable CalendarDay date, boolean selected) {
        selectedDate = date.getMonth()+1+"/"+date.getDay()+"/"+date.getYear();
        selectedsaildate = date.getMonth()+1+"/"+date.getDay()+"/"+date.getYear();
        String strDate = selectedDate;

        int i;
        Integer startkey = 0;
        Integer formatedSelectedDate = Integer.valueOf(global.parseDateToddMMyyyy(selectedDate, "MM/dd/yyyy", "yyyyMMdd"));
        Integer formatedSailDate = null;
        Boolean in_date = false;

        if(saildates!=null){
            for(i=0; i<saildates.size(); i++) {
                formatedSailDate = Integer.valueOf(global.parseDateToddMMyyyy(saildates.get(i), "MM/dd/yyyy", "yyyyMMdd"));
                if(saildates.get(i).equals(selectedDate)) {
                    selectedDateKey = i;
                    strDate = saildates.get(i);
                    in_date = true;
                }
                if(formatedSailDate < formatedSelectedDate){
                    startkey = i;
                }
            }
            if(!in_date){
                strDate = saildates.get(startkey);
                selectedDateKey = startkey;
            }
            getItineraryTurnInfo(strDate);
        }else {
            global.Alert("No result found.");
        }




    }


    public void Itinerary(JSONArray arrayData){

        Intent intent = new Intent();
        intent.putExtra("itinerary", String.valueOf(arrayData));
        intent.putExtra("saildates", saildates);
        intent.putExtra("selectedDateKey", String.valueOf(selectedDateKey));
        intent.putExtra("selectedsaildate", String.valueOf(selectedsaildate));
        setResult(10, intent);
        finish();
    }


    private boolean in_date(JSONArray data) throws JSONException {
        boolean result = false;
        String date_1 = global.parseDateToddMMyyyy(selectedDate, "MM/dd/yyyy", "MM/dd/yyyy");
        String date_2 = null;
        int i;
        for(i=0; i<data.length(); i++) {
            date_2 = global.parseDateToddMMyyyy(data.getString(i), "MM/dd/yyyy", "MM/dd/yyyy");

            Log.v("lester-log", date_1+" | "+date_2);
            if(date_1.equals(date_2)){
                result = true;
            }
        }
        return result;
    }


    public void getItineraryTurnInfo(String d){

        try {

            progressBar.setMessage("Retrieving Itinerary...");
            progressBar.show();

            Api.getItineraryTurnInfo(shipcode, d, new VolleyCallback() {
                @Override
                public void onSuccessResponse(String result) {
                    try {
                        JSONObject response = new JSONObject(result);

                        if (response.getBoolean("success")) {
                            JSONArray arrayData = response.getJSONArray("data");
                            JSONArray datesArray = arrayData.getJSONObject(0).getJSONArray("date");
                            //this function will only display results that in the dates
                            if(in_date(datesArray)){
                                Itinerary(arrayData);
                            }else {
                                global.Alert("No result found.");
                            }
//                            Itinerary(arrayData);
                        }else{
                            global.Alert("No result found.");
                        }
                        progressBar.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        global.Toast("Error!");
                        progressBar.dismiss();
                    }
                }

                @Override
                public void onFailedResponse(VolleyError result) {
                    global.Toast("Error!");
                    progressBar.dismiss();
                }
            });
        } catch (JSONException e){
            global.Toast("Error!");
            progressBar.dismiss();
        }

    }
}







