package com.rcclpmo.a7567.ilookup.Recycler;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rcclpmo.a7567.ilookup.DialogFragmentOptionSelection;
import com.rcclpmo.a7567.ilookup.MainActivity;
import com.rcclpmo.a7567.ilookup.R;
import com.rcclpmo.a7567.ilookup.RecyclerViewClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.callback.Callback;

public class ItemArrayAdapter extends RecyclerView.Adapter<ItemArrayAdapter.MyViewHolder> {



    public interface Callbacks {
        public void onButtonClicked(String titleKey);
    }


    private List<Item> itemList, itemsCopy;

    private Context context;

    private int type;
    private MainActivity onClick;


    public class MyViewHolder extends RecyclerView.ViewHolder{


        public TextView title;
        private View.OnClickListener onClickListener;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.item_title);
        }


    }

    public interface Callback {
        void onItemClick(int position);
    }


    public ItemArrayAdapter(List<Item> itemList) {
        this.itemList = itemList;
        this.itemsCopy =  new ArrayList<>();
        this.itemsCopy.addAll(itemList);
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int i) {
        final Item item = itemList.get(i);
        holder.title.setText(item.getName());
    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public JSONObject getItemCopy(int pos){
        JSONObject result = new JSONObject();
        try {
            result.put("name", itemList.get(pos).getName());
            result.put("code", itemList.get(pos).getCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }



    public void filter(String text) {
        itemList.clear();
        if(text.isEmpty()){
            itemList.addAll(itemsCopy);
        } else{
            text = text.toLowerCase();

            for(Item item: itemsCopy){
                if (item.getName().toLowerCase().contains(text)){
                    itemList.add(item);
                }
            }
        }
        notifyDataSetChanged();

    }



}