package com.rcclpmo.a7567.ilookup;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.rcclpmo.a7567.ilookup.Recycler.Item;
import com.rcclpmo.a7567.ilookup.Recycler.ItemArrayAdapter;
import com.rcclpmo.a7567.ilookup.Recycler.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class MainActivity extends AppCompatActivity
        implements RecyclerViewClickListener, NavigationView.OnNavigationItemSelectedListener {

    private DBHelper mydb ;
    private Global global ;
    public static String access_token;
    public static String uid;
    public static JSONObject user;
    private  ProgressDialog progressBar;
    LinearLayout Logoutbtn;
    Button ScheduleBtn;
    TextView ViewSailDates;

    TextView tvSearchShipNav;
    TextView tvSearchPortNav;

    Spinner ShipOption;
    final ArrayList<String> ship_names = new ArrayList<String>();
    final ArrayList<String> ship_codes = new ArrayList<String>();

    final ArrayList<String> port_codes = new ArrayList<String>();
    final ArrayList<String> port_names = new ArrayList<String>();

    String portcode;
    String portname;


    LinearLayout MainLayout;
    LinearLayout PortLayout;

    Integer SelectedShip = 0;
    public static Integer month;
    public static Integer year;

    public static String shipcode;
    public static String shipname;

    ArrayList<String> saildates;
    public int selectedDateKey;

    LinearLayout itineraryLayout;
    LinearLayout BottomButton;
    Integer layoutwidth = 0;

    LinearLayout ViewSailDatesBg;

    ScrollView TableContainer;

    String selectedsaildate;
    TableLayout tbody;
    Integer focusTo;

    String tabpage = "main";

    AlertDialog dialog;
    View Recycler_content = null;

    LinearLayout tvPortOption;
    TextView tvPortOptionText;


    //search inputs
    EditText searchView;
    LinearLayout searchContainer;
    LinearLayout searchContainer2;


    LinearLayout PortContentContainer;

    private List<Item> itemList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ItemArrayAdapter iAdapter;


//    DialogFragmentOptionSelection PortDialog;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mydb = new DBHelper(this);
        global = new Global(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        setSupportActionBar(toolbar);
        mTitle.setText(toolbar.getTitle());

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);

        float density  = getResources().getDisplayMetrics().density;
        layoutwidth  = Math.round(outMetrics.widthPixels / density);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Logoutbtn = navigationView.findViewById(R.id.logout);

        Logoutbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logout();
            }
        });

        LoaderInit();

        String user_data = getIntent().getExtras().getString("user_data");

        View header = navigationView.getHeaderView(0) ;


        tbody = (TableLayout) findViewById( R.id.BodyTable );

        TextView userNameView = (TextView) header.findViewById(R.id.user_name);
        TextView userEmailView = (TextView) header.findViewById(R.id.user_email);



        Logoutbtn = (LinearLayout) header.findViewById(R.id.logout);


        tvPortOption = (LinearLayout) findViewById(R.id.tvPortOption);
        tvPortOptionText = (TextView) findViewById(R.id.tvPortOptionText);

        ImageView userProfilePic = (ImageView) header.findViewById(R.id.profilepic);


        ShipOption = (Spinner) findViewById(R.id.dropdownShip);

        PortContentContainer = (LinearLayout) findViewById(R.id.PortContentContainer);


        TableContainer = (ScrollView) findViewById(R.id.TableContainer);

        ViewSailDatesBg = (LinearLayout) findViewById(R.id.ViewSailDatesBg);
        ViewSailDates = (TextView) findViewById(R.id.ViewSailDates);

        ViewSailDatesBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetCalendar("button");
            }
        });

        ViewSailDates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetCalendar("button");
            }
        });


        tvPortOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFilterPort();
            }
        });

        ScheduleBtn = (Button) findViewById(R.id.schedulebtn);
        ScheduleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetCalendar("button");
            }

        });


        try {
            JSONObject obj_user = new JSONObject(user_data);
            access_token = obj_user.getString("access_token");
            user = obj_user.getJSONObject("user");
            uid = user.getString("id");
            userNameView.setText(user.getString("first_name")+" "+user.getString("last_name"));
            userEmailView.setText(user.getString("email"));
            getShips();

        } catch (JSONException e) {
            e.printStackTrace();
            global.Alert("Something went wrong!");
            Logout();
        }


        BottomButton = (LinearLayout) findViewById(R.id.BottomButton);
        itineraryLayout = (LinearLayout) findViewById(R.id.itineraryLayout);
        ShipOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                SelectedShip = position;
                GetCalendar("default");
                SetDateButton();
                itineraryLayout.setVisibility(View.INVISIBLE);
                BottomButton.setVisibility(View.INVISIBLE);
                saildates = new ArrayList<>();
            }
            public void onNothingSelected(AdapterView<?> parent)
            {
                global.Alert("nothing selected.");
            }
        });



        MainLayout = (LinearLayout) findViewById(R.id.MainLayout);
        PortLayout = (LinearLayout) findViewById(R.id.PortLayout);

        MainLayout.setVisibility(View.VISIBLE);
        PortLayout.setVisibility(View.GONE);

        setNavHeader();
        initRecycler();

//        PortDialog = DialogFragmentOptionSelection.createInstance(new Bundle());

    }

    private void search(final EditText searchView) {

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                iAdapter.filter(String.valueOf(searchView.getText()));
            }
        });


//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                iAdapter.filter(newText);
//                return true;
//            }
//        });
    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initRecycler(){

        Toolbar toolbar = null;

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomDialog);
        LayoutInflater inflater = this.getLayoutInflater();
        View content = inflater.inflate(R.layout.search_by_port_filter, null);
        toolbar = content.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_button_white);
        toolbar.setTitle(getString(R.string.selectport));

        ImageView searchMagIcon = (ImageView) content.findViewById(R.id.search_mag_icon);
        ImageView searchClear = (ImageView) content.findViewById(R.id.search_clear);

        searchView = (EditText) content.findViewById(R.id.search_view);
        searchContainer = (LinearLayout) content.findViewById(R.id.search_container);
        searchContainer2 = (LinearLayout) content.findViewById(R.id.search_container_2);


        toolbar.setNavigationOnClickListener(new Toolbar.OnClickListener(){
            @Override
            public void onClick(View v) {
                searchButtons(false);
            }
        });


        searchMagIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchButtons(true);
            }
        });

        searchClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchButtons(false);
            }
        });

        searchContainer.setVisibility(View.GONE);

        search(searchView);
        builder.setView(content);

        recyclerView = (RecyclerView) content.findViewById(R.id.recycler_view);

        iAdapter = new ItemArrayAdapter(preparePortItemList());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(iAdapter);


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        setPort(position, iAdapter.getItemCopy(position));
                        searchButtons(false);
                        dialog.cancel();

                    }
                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
        builder.create();
        dialog = builder.create();

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_BACK && !event.isCanceled()) {
                    searchButtons(false);
                    return false;
                }
                return false;
            }
        });
    }


    @SuppressLint("WrongConstant")
    public void searchButtons(Boolean action){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = dialog.getCurrentFocus();
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Integer searchbar = searchContainer2.getVisibility();
        searchView.setText("");
        if(action){
            searchContainer.setVisibility(View.VISIBLE);
            searchContainer2.setVisibility(View.GONE);
            searchView.requestFocus();
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }else{

            if(searchbar > 0 ){
                searchContainer.setVisibility(View.GONE);
                searchContainer2.setVisibility(View.VISIBLE);
                iAdapter.filter(String.valueOf(searchView.getText()));
            }else{
                dialog.cancel();
            }
        }
    }


    @SuppressLint("ResourceType")
    private void setFilterPort() {
        iAdapter.filter("");
        iAdapter.notifyDataSetChanged();
        dialog.show();
//        PortDialog.show(getSupportFragmentManager(),"port");
    }


    private void preparePortItem() {
        Item item;
        for (int i = 0; i < port_names.size(); i++) {
            String portname = port_names.get(i);
            String portcode = port_codes.get(i);
            item = new Item(portname, portcode);
            itemList.add(item);
        }
        iAdapter.notifyDataSetChanged();
    }

    private List preparePortItemList() {
        Item item;
        for (int i = 0; i < port_names.size(); i++) {
            String portname = port_names.get(i);
            String portcode = port_codes.get(i);
            item = new Item(portname, portcode);
            itemList.add(item);

            Log.v("lester-list-port", String.valueOf(item));
        }
        return itemList;
    }

    public void setPort(final Integer pos, final JSONObject itemcopy){


        try {
            portcode = itemcopy.getString("code");
            portname = itemcopy.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }





        try {

            progressBar.setMessage("Downloading Port info...");
            progressBar.show();

            Api.getTurnInfoByPort(portcode, new VolleyCallback() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onSuccessResponse(String result) {
                    try {
                        JSONObject response = new JSONObject(result);

                        if (response.getBoolean("success")) {
                            JSONArray arrayData = response.getJSONArray("data");
                            organizePortData(arrayData);
                        }else{
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "No result found.", Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            PortContentContainer.setVisibility(View.INVISIBLE);
                        }
                        tvPortOptionText.setText(portname+"("+portcode+")");
                        progressBar.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        global.Toast("Error!");
                        progressBar.dismiss();
                    }
                }

                @Override
                public void onFailedResponse(VolleyError result) {
                    Snackbar snackbar = Snackbar
                            .make(findViewById(android.R.id.content), "No internet connection!", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    setPort(pos, itemcopy);
                                }
                            });
                    snackbar.show();
                    progressBar.dismiss();


                }

            });
        } catch (JSONException e){
            global.Toast("Error!");
            progressBar.dismiss();
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceAsColor")
    private void organizePortData(JSONArray data){

        TableLayout thead = (TableLayout) findViewById(R.id.PortTableHead);
        thead.removeAllViews();
        try {

            PortContentContainer.setVisibility(View.VISIBLE);
            final JSONObject shiport = data.getJSONObject(0);
            final JSONArray portShipCode = shiport.getJSONArray("ShipCode");
            final JSONArray portSailMonths = shiport.getJSONArray("SailMonths");
            final JSONArray portSailCount = shiport.getJSONArray("SailCount");

            TableRow hrow=new TableRow(this);
            TextView hsctv = (TextView)getLayoutInflater().inflate(R.layout.template_port_header_shipcode, null);
            hrow.addView(hsctv);

            for (int a = 0; a < portSailMonths.length(); a++){
                TextView htv = (TextView)getLayoutInflater().inflate(R.layout.template_port_header, null);
                htv.setText(portSailMonths.getString(a));
                hrow.addView(htv);
            }
            thead.addView(hrow);


            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            PopulatePortBody(shiport);
                        }
                    }, 1);



        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    public void PopulatePortBody(JSONObject shiport){

        TableLayout thead = (TableLayout) findViewById(R.id.PortTableHead);
        TableRow theadwidth = (TableRow) thead.getChildAt(0);
        Integer getcount = theadwidth.getChildCount();

        ArrayList<Integer> RowWidths = new ArrayList<Integer>();
        ArrayList<Integer> RowTotals = new ArrayList<Integer>();

        Integer shipcode_height = theadwidth.getChildAt(0).getHeight();

        for (int a = 0; a < getcount; a++){
            RowWidths.add(theadwidth.getChildAt(a).getWidth());
            RowTotals.add(0);
        }

        Log.v("lester-width", String.valueOf(RowWidths));
        try {
        JSONArray portShipCode = shiport.getJSONArray("ShipCode");
        JSONArray portSailMonths = shiport.getJSONArray("SailMonths");
        JSONArray portSailCount = shiport.getJSONArray("SailCount");

        TableLayout tbodyport = (TableLayout) findViewById(R.id.PortBodyTable);
        tbodyport.removeAllViews();
        for (int i = 0; i < portShipCode.length(); i++){
            String portShipCodeString = portShipCode.getString(i);
            TableRow brow=new TableRow(this);

            //get shipcode
            TextView btv = (TextView)getLayoutInflater().inflate(R.layout.template_port_body_shipcode, null);
            btv.setText(portShipCodeString);
            btv.setWidth(RowWidths.get(0));
            brow.addView(btv);

            //get the port count
            for (int ii = 0; ii < portSailMonths.length(); ii++){
                String portSailCountString = (String) portSailCount.getJSONArray(ii).getString(i);
                TextView bdtv = (TextView)getLayoutInflater().inflate(R.layout.template_port_body, null);
                bdtv.setText(portSailCountString);
                RowTotals.set(ii+1, RowTotals.get(ii+1)+Integer.valueOf(portSailCountString));
                bdtv.setWidth(RowWidths.get(ii+1));
                brow.addView(bdtv);
            }
            tbodyport.addView(brow);

        }

            //set total
            TableRow brow=new TableRow(this);
            TextView btv = (TextView)getLayoutInflater().inflate(R.layout.template_port_body_shipcode, null);
            btv.setText("Total");
            btv.setWidth(RowWidths.get(0));
            brow.addView(btv);

            for (int aa = 0; aa < portSailMonths.length(); aa++){
                TextView bdtv = (TextView)getLayoutInflater().inflate(R.layout.template_port_body, null);
                bdtv.setText(String.valueOf(RowTotals.get(aa+1)));
                bdtv.setWidth(RowWidths.get(aa+1));
                brow.addView(bdtv);
            }

            tbodyport.addView(brow);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setNavHeader(){
        tvSearchShipNav = (TextView) findViewById(R.id.tvSearchShipNav);
        tvSearchShipNav.setTextColor(getApplication().getResources().getColor(R.color.colorWhite));
        tvSearchShipNav.setBackgroundResource(R.drawable.selected_header_bg_left);

        tvSearchPortNav = (TextView) findViewById(R.id.tvSearchPortNav);
        tvSearchPortNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTab("port");

            }
        });
        tvSearchShipNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTab("main");
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void changeTab(String tab){
        if(tab.equals("main")){
            PortLayout.setVisibility(View.GONE);
            MainLayout.setVisibility(View.VISIBLE);
            tvSearchShipNav.setTextColor(getApplication().getResources().getColor(R.color.colorWhite));
            tvSearchShipNav.setBackgroundResource(R.drawable.selected_header_bg_left);
            tvSearchPortNav.setTextColor(getApplication().getResources().getColor(R.color.lightGreen));
            tvSearchPortNav.setBackgroundResource(0);
            tabpage = "main";
        }else{

            MainLayout.setVisibility(View.GONE);
            PortLayout.setVisibility(View.VISIBLE);
            tvSearchPortNav.setTextColor(getApplication().getResources().getColor(R.color.colorWhite));
            tvSearchPortNav.setBackgroundResource(R.drawable.selected_header_bg_right);
            tvSearchShipNav.setTextColor(getApplication().getResources().getColor(R.color.lightGreen));
            tvSearchShipNav.setBackgroundResource(0);

            if(tabpage.equals("main")){
                setFilterPort();
            }

            tabpage = "port";
        }




    }

    public void GetCalendar(String type){
        TextView ViewSailDates = (TextView) findViewById(R.id.ViewSailDates);

        if(SelectedShip!=0){
            ViewSailDatesBg.setBackgroundResource(R.drawable.saildates_active);
            ViewSailDates.setTextColor(getApplication().getResources().getColor(R.color.colorWhite));
            shipcode = ship_codes.get(SelectedShip).toString();
            shipname = ship_names.get(SelectedShip).toString();
            Intent i = new Intent(MainActivity.this, CalendarActivity.class);

            String defaultdate;
            if (saildates.size()>0 && !type.equals("default")){
                defaultdate = saildates.get(selectedDateKey);
            }else {
                defaultdate = null;
            }
            i.putExtra("defaultdate", defaultdate);
            i.putExtra("defaultdateselected", selectedsaildate);
            startActivityForResult(i, 10);

        }else{
            ViewSailDatesBg.setBackgroundResource(R.drawable.saildates_bg);
            ViewSailDates.setTextColor(getApplication().getResources().getColor(R.color.AppBarColor));
        }
    }

    private void SetDateButton() {
        Date d = Calendar.getInstance().getTime();

        SimpleDateFormat m = new SimpleDateFormat("MM");
        month = Integer.valueOf(m.format(d));

        SimpleDateFormat y = new SimpleDateFormat("yyyy");
        year = Integer.valueOf(y.format(d));

        SimpleDateFormat df = new SimpleDateFormat("yyyy MMM dd");
        String formattedDate = df.format(d);
        SimpleDateFormat day = new SimpleDateFormat("EEEE");
        String days = day.format(d).toUpperCase();
        String date = "<b>"+days+"</b>, "+formattedDate;
        ScheduleBtn.setText(Html.fromHtml(date));
    }



    public void LoaderInit(){
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setCanceledOnTouchOutside(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setMax(100);
        progressBar.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_BACK && !event.isCanceled()) {
                    if(progressBar.isShowing()) {
                        //your logic here for back button pressed event
                        Api.CancelCall();
                        progressBar.dismiss();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public void getShips(){

        try {

            progressBar = new ProgressDialog(this);
            progressBar.setCancelable(false);
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setMax(100);
            progressBar.setMessage("Downloading Ships...");
            progressBar.show();

            Api.Ship(uid, access_token, new VolleyCallback() {
                @Override
                public void onSuccessResponse(String result) {
                    try {

                        JSONObject response = new JSONObject(result);
                        if(response.getBoolean("success")){

                            ship_names.add("Select Ship");
                            ship_codes.add("0");

                            JSONArray arrayData = response.getJSONArray("data");
                            int i;
                            for(i=0; i<arrayData.length(); i++){
                                JSONObject shipObject = new JSONObject(arrayData.getString(i));
                                ship_names.add(shipObject.getString("shipname"));
                                ship_codes.add(shipObject.getString("shipcode"));
                            }
                            getPorts();
                        }else{
                            if(response.getString("message").equals("Something went wrong!")){
                                Snackbar snackbar = Snackbar
                                        .make(findViewById(android.R.id.content), "No internet connection!", Snackbar.LENGTH_INDEFINITE)
                                        .setAction("RETRY", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                getShips();
                                            }
                                        });
                                snackbar.show();
                            }
                            progressBar.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        global.Alert("No results found.");
                        progressBar.dismiss();
                    }
                }

                @Override
                public void onFailedResponse(VolleyError result) {
                    Snackbar snackbar = Snackbar
                            .make(findViewById(android.R.id.content), "No internet connection!", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getShips();
                                }
                            });
                    snackbar.show();
                    progressBar.dismiss();
                }
            });
        } catch (JSONException e) {
            global.Alert("Error!");
        }
    }


    public void getPorts(){

        try {
            progressBar.setMessage("Downloading Ports...");
            Api.Port(new VolleyCallback() {
                @Override
                public void onSuccessResponse(String result) {
                    try {

                        JSONObject response = new JSONObject(result);
                        if(response.getBoolean("success")){
                            JSONArray arrayData = response.getJSONArray("data");
                            int i;
                            Item item;
                            for(i=0; i<arrayData.length(); i++){
                                JSONObject portObject = new JSONObject(arrayData.getString(i));
                                port_names.add(portObject.getString("portname"));
                                port_codes.add(portObject.getString("portcode"));
                            }
//                            PortDialog.setItemList(itemList);
//                            mydb.insertPort(String.valueOf(port_names));
                            PopulateShip(ship_names);
                            progressBar.dismiss();
                            initRecycler();
                            LoaderInit();
                        }else{
                            if(response.getString("message").equals("Something went wrong!")){
                                Snackbar snackbar = Snackbar
                                        .make(findViewById(android.R.id.content), "No internet connection!", Snackbar.LENGTH_INDEFINITE)
                                        .setAction("RETRY", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                getShips();
                                            }
                                        });
                                snackbar.show();
                            }
                            progressBar.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        global.Alert("No results found.");
                        progressBar.dismiss();
                    }
                }

                @Override
                public void onFailedResponse(VolleyError result) {
                    Snackbar snackbar = Snackbar
                            .make(findViewById(android.R.id.content), "No internet connection!", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getShips();
                                }
                            });
                    snackbar.show();
                    progressBar.dismiss();
                }
            });
        } catch (JSONException e) {
            global.Alert("Error!");
        }

    }

    public void PopulateShip(ArrayList<String> array_ships){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, array_ships);
        ShipOption.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        global.Alert("test");
        if (id == R.id.logout) {
            Logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==10){
            try {
                JSONArray jsonItinerary = new JSONArray(data.getStringExtra("itinerary"));
                ArrayList string_saildates = data.getStringArrayListExtra("saildates");
                selectedDateKey = Integer.parseInt(data.getStringExtra("selectedDateKey"));
                selectedsaildate = data.getStringExtra("selectedsaildate");
                saildates = new ArrayList<String>(string_saildates);
                PopulateItinerary(jsonItinerary);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceAsColor")
    public  void PopulateItinerary(JSONArray ObjectData) throws JSONException {

        itineraryLayout.setVisibility(View.VISIBLE);

        TableLayout thead = (TableLayout) findViewById( R.id.HeaderTable);
        TableRow theadRow = (TableRow) thead.getChildAt(0);


        Integer headerWidth_1 = Integer.valueOf(theadRow.getChildAt(0).getWidth());
        Integer headerWidth_2 = Integer.valueOf(theadRow.getChildAt(1).getWidth());
        Integer headerWidth_3 = Integer.valueOf(theadRow.getChildAt(2).getWidth());
        Integer headerWidth_4 = Integer.valueOf(theadRow.getChildAt(3).getWidth());
        Integer headerWidth_5 = Integer.valueOf(theadRow.getChildAt(4).getWidth());
        Integer headerWidth_6 = Integer.valueOf(theadRow.getChildAt(5).getWidth());

        tbody.removeAllViews();

        JSONObject data = ObjectData.getJSONObject(0);

        JSONArray voyageno_data = data.getJSONArray("voyageno");
        JSONArray date_data = data.getJSONArray("date");
        JSONArray weekday_data = data.getJSONArray("weekday");
        JSONArray portofcall_data = data.getJSONArray("portofcall");
        JSONArray arrival_data = data.getJSONArray("arrival");
        JSONArray departure_data = data.getJSONArray("departure");
        JSONArray td_data = data.getJSONArray("td");
        JSONArray stats_data = data.getJSONArray("stats");


        focusTo = 0;

        for(int i=0;i<voyageno_data.length();i++)
            {
                String date         = date_data.getString(i);
                String weekday      = weekday_data.getString(i);
                String portofcall   = portofcall_data.getString(i);
                String arrival      = arrival_data.getString(i);
                String departure    = departure_data.getString(i);
                String td           = td_data.getString(i);

                TableRow row=new TableRow(this);
                if(date.equals(global.parseDateToddMMyyyy(selectedsaildate, "MM/dd/yyyy", "MM/dd/yyyy"))){
                    row.setBackgroundResource(R.drawable.tablerow_bg_selected);
                    focusTo = i;
                }else{
                    row.setBackgroundResource(R.drawable.tablerow_bg);
                }

                row.setPadding(5,15,5,15);

                //set date
                TextView tv1=new TextView(this);
                TextView tv2=new TextView(this);
                TextView tv3=new TextView(this);
                TextView tv4=new TextView(this);
                TextView tv5=new TextView(this);
                TextView tv6=new TextView(this);

                String [] sdateParts= date.split("/");
                Integer dayparts    = Integer.valueOf(sdateParts[1]);
                Integer monthparts  = Integer.valueOf(sdateParts[0]);




                tv1.setText(monthparts+"/"+dayparts);
                tv2.setText(weekday);
                tv3.setText(portofcall.replace("/", ",").replace("|", ","));
                tv4.setText(ConvertTime(String.valueOf(arrival),td));
                tv5.setText(ConvertTime(String.valueOf(departure),td));
                tv6.setText(td);

//                tv1.setTextAppearance(R.style.iLookUpItineraryTbody);
//                tv2.setTextAppearance(R.style.iLookUpItineraryTbody);
//                tv3.setTextAppearance(R.style.iLookUpItineraryTbody);
//                tv4.setTextAppearance(R.style.iLookUpItineraryTbody);
//                tv5.setTextAppearance(R.style.iLookUpItineraryTbody);
//                tv6.setTextAppearance(R.style.iLookUpItineraryTbody);



                tv1.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                tv2.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                tv3.setGravity(Gravity.LEFT | Gravity.BOTTOM);
                tv4.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                tv5.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                tv6.setGravity(Gravity.CENTER | Gravity.BOTTOM);

                tv1.setWidth(headerWidth_1);
                tv2.setWidth(headerWidth_2);
                tv3.setWidth(headerWidth_3);
                tv4.setWidth(headerWidth_4);
                tv5.setWidth(headerWidth_5);
                tv6.setWidth(headerWidth_6);

                tv3.setSingleLine(false);
                if(layoutwidth<=330){
                    tv1.setTextSize(12);
                    tv2.setTextSize(12);
                    tv3.setTextSize(12);
                    tv4.setTextSize(12);
                    tv5.setTextSize(12);
                    tv6.setTextSize(12);
                }else if(layoutwidth<=400){
                    tv1.setTextSize(15);
                    tv2.setTextSize(15);
                    tv3.setTextSize(15);
                    tv4.setTextSize(15);
                    tv5.setTextSize(15);
                    tv6.setTextSize(15);
                }else {
                    tv1.setTextSize(17);
                    tv2.setTextSize(17);
                    tv3.setTextSize(16);
                    tv4.setTextSize(17);
                    tv5.setTextSize(17);
                    tv6.setTextSize(17);
                }

                tv1.setTextColor(R.color.CalendarFont);
                tv2.setTextColor(R.color.CalendarFont);
                tv3.setTextColor(R.color.CalendarFont);
                tv4.setTextColor(R.color.CalendarFont);
                tv5.setTextColor(Color.parseColor("#fb0019"));
                tv6.setTextColor(R.color.CalendarFont);

                row.addView(tv1);
                row.addView(tv2);
                row.addView(tv3);
                row.addView(tv4);
                row.addView(tv5);
                row.addView(tv6);
                tbody.addView(row);
            }

            new android.os.Handler().postDelayed(
            new Runnable() {
                public void run() {
                    Integer rowcount = tbody.getChildCount();
                    Integer x;
                    Integer y = 0;

                    for(x=0;x<rowcount;x++)
                    {
                        TableRow tbodyRow = (TableRow) tbody.getChildAt(x);
                        if(focusTo>x){
                            y += tbodyRow.getHeight();
                        }
                    }
                    TableContainer.scrollTo(0, y);
                }
            }, 1);

        String prev  = String.valueOf(stats_data.get(0));
        String next  = String.valueOf(stats_data.get(stats_data.length() - 1));

        Button PrevButton = (Button) findViewById(R.id.PrevButton);
        Button NextButton = (Button) findViewById(R.id.NextButton);

        PrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer selkey = selectedDateKey;
                if(selkey>0){
                    selkey -=1;
                }else {
                    selkey = saildates.size()-1;
                }
                getItineraryTurnInfo(selkey);
            }
        });
        NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer selkey = selectedDateKey;
                if(selkey < saildates.size()-1){
                    selkey +=1;
                }else {
                    selkey = 0;
                }
                getItineraryTurnInfo(selkey);
            }
        });

        BottomButton.setVisibility(View.VISIBLE);

        LinearLayout BottomButton = (LinearLayout) findViewById(R.id.BottomButton);
        BottomButton.setVisibility(View.VISIBLE);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy"); // here set the pattern as you date in string was containing like date/month/year
            Date d = sdf.parse(saildates.get(selectedDateKey));
            SetDateButtonResult(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        RevealLabel();
    }



    public void RevealLabel(){
        TextView BottomLabel = (TextView) findViewById(R.id.BottomLabel);
        if(layoutwidth>=500){
            BottomLabel.setVisibility(View.VISIBLE);
        }else{
            BottomLabel.setVisibility(View.GONE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(tabpage.equals("port")){
                changeTab("main");
            }else{
                super.onBackPressed();
            }
        }


    }

    public void getItineraryTurnInfo(final Integer selkey){
        String d = saildates.get(selkey);
        try {

            progressBar.setMessage("Retrieving Itinerary...");
            progressBar.show();

            Api.getItineraryTurnInfo(shipcode, d, new VolleyCallback() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onSuccessResponse(String result) {
                    try {
                        JSONObject response = new JSONObject(result);

                        if (response.getBoolean("success")) {
                            JSONArray arrayData = response.getJSONArray("data");
                            selectedDateKey = selkey;
                            PopulateItinerary(arrayData);
                        }else{
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Something went wrong! Please try again.", Snackbar.LENGTH_SHORT);
                            snackbar.show();
                        }
                        progressBar.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        global.Toast("Error!");
                        progressBar.dismiss();
                    }
                }

                @Override
                public void onFailedResponse(VolleyError result) {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Something went wrong! Please try again.", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            });
        } catch (JSONException e){
            global.Toast("Error!");
            progressBar.dismiss();
        }
    }

    private void SetDateButtonResult(Date d) {

        SimpleDateFormat m = new SimpleDateFormat("MM");
        month = Integer.valueOf(m.format(d));

        SimpleDateFormat y = new SimpleDateFormat("yyyy");
        year = Integer.valueOf(y.format(d));

        SimpleDateFormat df = new SimpleDateFormat("yyyy MMM dd");
        String formattedDate = df.format(d);
        SimpleDateFormat day = new SimpleDateFormat("EEEE");
        String days = day.format(d).toUpperCase();
        String date = "<b>"+days+"</b>, "+formattedDate;
        ScheduleBtn.setText(Html.fromHtml(date));
    }


    private String ConvertTime(String s, String td){
        String result = "";
        if(td.equals("C")){
            result = " - ";
        }else{
            DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
            Date d = null;
            try {
                d = f1.parse(s);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat f2 = new SimpleDateFormat("hh:mm a");
            result = f2.format(d);
        }
        return result;
    }

    public void Logout(){
        mydb.deleteLogin();
        Intent i = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onRecyclerItemClick(int position, Object item) {
        global.Alert(String.valueOf(position));
    }
}
