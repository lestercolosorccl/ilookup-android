package com.rcclpmo.a7567.ilookup;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.style.ForegroundColorSpan;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Collection;
import java.util.HashSet;

public class DateDecorator implements DayViewDecorator {
    private int color = 0;
    private String type;
    private final HashSet<CalendarDay> dates;
    private ColorDrawable drawable;
    private Context context;

    public DateDecorator(Context context, int color,String type, Collection<CalendarDay> dates) {
        this.context = context;
        this.color = color;
        this.type = type;
        this.dates = new HashSet<>(dates);
        drawable = new ColorDrawable(color);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {

//        view.addSpan(new DotSpan(20, Color.RED));
//        view.addSpan(new DotSpan(10, color));
//        view.addSpan(new tex(10, color));
//        view.addSpan(new ForegroundColorSpan(color));
//        view.addSpan(new RelativeSizeSpan(1.8f)); // for TextSize
        if(type=="current_date"){
            view.addSpan(new ForegroundColorSpan(Color.WHITE)); // for TextColor
        }else if(type=="sail_date"){
            Drawable highlightDrawable = this.context.getResources().getDrawable(R.drawable.circle_yellow);
            view.setBackgroundDrawable(highlightDrawable);
        }else if(type=="selected_date"){
            Drawable highlightDrawable = this.context.getResources().getDrawable(R.drawable.circle_red);
            view.setBackgroundDrawable(highlightDrawable);
        }else{
            Drawable highlightDrawable = this.context.getResources().getDrawable(R.color.CalendarBg);
            view.setBackgroundDrawable(highlightDrawable);
        }


    }
}
