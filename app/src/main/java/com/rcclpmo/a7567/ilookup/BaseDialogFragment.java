package com.rcclpmo.a7567.ilookup;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

/**
 * Created by 7567 on 21/05/2018.
 */

public abstract class BaseDialogFragment extends android.support.v4.app.DialogFragment implements View.OnClickListener, ResponseHandler {

    public MyApplication applicationClass;

    private View view;
    private int layoutId = 0;
    private int animationEnter = R.anim.slide_up;
    private int animationExit = R.anim.slide_down;

    TextView tvOptionTitle;

    public BaseDialogFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
            initMain();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(layoutId, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        //passing of data through arguments
        initData(getArguments());

        //for initialization of views
        initViews(view, savedInstanceState);

        if (hasToolbar()){
            setUpToolbar(view);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        showParent();
    }

    public abstract void initMain();
    public abstract void initData(Bundle bundle);
    /**
     * This method initializes views included in the layout
     * @param view
     * @param savedInstanceState
     */
    /****/
    protected abstract boolean hasToolbar();

    protected abstract boolean isCustomDialog();


    /*****
     * This method handle common API implementation in all dialog fragment
     * @param requestCode
     */
    protected abstract void handleCommonAPI(int requestCode);

    public abstract void initViews(View view, Bundle savedInstanceState);

    public void setToolbarTitle(String title){
        tvOptionTitle.setText(title);
    }

    public void setUpToolbar(View view){
//        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
//        ivBack = (ImageView) view.findViewById(R.id.ivBackButton);
//        ivBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideParent();
//            }
//        });
    }

    /**
     * Animates dialogFragment onShow
     */
    private void showParent() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                view.startAnimation(AnimationUtils.loadAnimation(getActivity(), animationEnter));
                view.setVisibility(View.VISIBLE);
//                viewDidLoad();
            }
        }, 500);

    }

    public void viewDidLoad() {

    }

    /**
     * Animates dialogFragment onStop
     */
    public void hideParent() {
        view.startAnimation(AnimationUtils.loadAnimation(getActivity(), animationExit));
        view.setVisibility(View.GONE);
        dismiss();
    }


    /**
     * Sets the layout id of the activity
     * @param layoutId
     */
    public void setLayoutId(int layoutId) {

        this.layoutId = layoutId;

    }

    /**
     * Sets view to be animated
     * @param view
     */
    public void setViewToAnimate(View view) {

        this.view = view;

    }

    /**
     * Sets entrance of animation
     * @param animationEnter
     */
    public void setAnimationEnter(int animationEnter) {

        this.animationEnter = animationEnter;

    }

    /**
     * Sets exit of animation
     * @param animationExit
     */
    public void setAnimationExit(int animationExit) {

        this.animationExit = animationExit;

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onSuccess(int requestCode, Object object) {
        switch (requestCode){

            default:break;
        }
    }

    @Override
    public void onFailed(int requestCode, String message) {

    }

}