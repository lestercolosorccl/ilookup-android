package com.rcclpmo.a7567.ilookup;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.rcclpmo.a7567.ilookup.Recycler.Item;
import com.rcclpmo.a7567.ilookup.Recycler.ItemArrayAdapter;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by 7567 on 21/05/2018.
 */

public class DialogFragmentOptionSelection extends BaseDialogFragment implements RecyclerViewClickListener {

    private List<Item> itemList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ItemArrayAdapter iAdapter;
    private DBHelper mydb;



    @Override
    public void initMain() {
        mydb = new DBHelper(getContext());
        setLayoutId(R.layout.search_by_port_filter);


    }

    @Override
    public void initData(Bundle bundle) {
        itemList = new ArrayList<>();
        if (bundle!=null){
        }
    }

    public static DialogFragmentOptionSelection createInstance(@Nullable Bundle data){

        DialogFragmentOptionSelection fragment = new DialogFragmentOptionSelection();
        fragment.setArguments(data);

        return fragment;
    }

    @Override
    protected boolean hasToolbar() {
        return false;
    }

    @Override
    protected boolean isCustomDialog() {
        return true;
    }

    @Override
    protected void handleCommonAPI(int requestCode) {

    }

    @Override
    public void initViews(View view, Bundle savedInstanceState) {
        View viewToAnimate = view.findViewById(R.id.searchFilterDialog);
        setViewToAnimate(viewToAnimate);

        recyclerView = view.findViewById(R.id.recycler_view);

//        setAnimationEnter(R.anim.slide_up);
//        setAnimationExit(R.anim.slide_down);
        initRecyclerView();

    }

    private List preparePortItemList() {
        List<Item> portlist = new ArrayList<>();
        String data = mydb.getPortData();

        ArrayList<String> myList = new ArrayList<>(Arrays.asList(data.split(",")));

//        List<String> myList = Arrays.asList(data);

//        Item item;
//        for (int i = 0; i < myList.size(); i++) {
//            String portname = myList.get(i);
//            String portname = myList.get(i);
//            item = new Item(portname);
//            portlist.add(item);
//        }

        return portlist;
    }


    private void initRecyclerView(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        iAdapter = new ItemArrayAdapter(getActivity(), preparePortItemList(), this);
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setAdapter(iAdapter);
//        recyclerView.setHasFixedSize(true);
    }



    @Override
    public void onRecyclerItemClick(int position, Object item) {

    }
}
