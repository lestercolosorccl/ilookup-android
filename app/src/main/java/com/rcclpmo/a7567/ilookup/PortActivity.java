package com.rcclpmo.a7567.ilookup;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;



    public class PortActivity extends AppCompatActivity
            implements NavigationView.OnNavigationItemSelectedListener {

    private DBHelper mydb ;
    private Global global ;

    TextView tvSearchShipNav;
    TextView tvSearchPortNav;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_port);

        mydb = new DBHelper(this);
        global = new Global(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        setNavHeader();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setNavHeader(){
        tvSearchPortNav = (TextView) findViewById(R.id.tvSearchPortNav);
        tvSearchPortNav.setTextAppearance(R.style.SelectedHeader);
        tvSearchPortNav.setBackgroundResource(R.drawable.selected_header_bg_right);

        tvSearchShipNav = (TextView) findViewById(R.id.tvSearchShipNav);

        tvSearchShipNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition( R.anim.right_in, R.anim.right_out);
            }
        });
    }



        public void Logout(){
            mydb.deleteLogin();
            Intent i = new Intent(PortActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
        @Override
        public void onBackPressed() {
    //        super.onBackPressed();
            finish();
            overridePendingTransition( R.anim.right_in, R.anim.right_out);
        }

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.logout) {
                Logout();
            }
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }




    }
