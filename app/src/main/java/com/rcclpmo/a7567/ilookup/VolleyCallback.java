package com.rcclpmo.a7567.ilookup;

import com.android.volley.VolleyError;

public interface VolleyCallback {
    void onSuccessResponse(String result);
    void onFailedResponse(VolleyError result);
}
