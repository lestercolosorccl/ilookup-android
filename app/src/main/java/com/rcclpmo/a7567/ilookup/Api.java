package com.rcclpmo.a7567.ilookup;


import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class Api extends Application {

    public static String ServerURL = BuildConfig.SERVER_URL;
    public static String accessToken = "d4d1bebc4185a97033c34d880f679511775ef531ca294f3c8b17a0b9e45634";
    private static final String TAG = "API";


    public static Object Login(final String email, final String password, final VolleyCallback callback) throws JSONException {

        String url =  ServerURL+"api/mobile/Auth/login";
        String data = null;

        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("lester-api", "success");
                callback.onSuccessResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFailedResponse(error);
            }
        }) {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> postparams = new HashMap<String, String>();
                postparams.put("email", email);
                postparams.put("password", password);
                return postparams;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(sr,TAG);
        return data;
    }

    public static void CancelCall(){
        MyApplication.getInstance().cancelAllRequests(TAG);
    }


    public static Object Ship(final String uid, final String access_token, final VolleyCallback callback) throws JSONException {
        String url =  ServerURL+"api/ilookup/ILookUp/getShip";
        String data = null;

        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onSuccessResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFailedResponse(error);
            }
        }) {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> postparams = new HashMap<String, String>();
                postparams.put("uid", uid);
                postparams.put("access_token", access_token);
                postparams.put("brandname", "android");
                return postparams;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(sr,TAG);
        return data;
    }

    public static Object SailDates(final String shipcode, final Integer year, final Integer month, final VolleyCallback callback) throws JSONException {

        String url =  ServerURL+"api/ilookup/ILookUp/getListDates";
        String data = null;

        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onSuccessResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFailedResponse(error);
            }
        }) {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> postparams = new HashMap<String, String>();
                postparams.put("brandcode", "ilookup");
                postparams.put("shipcode", shipcode);
//                postparams.put("month", String.valueOf(month));
                postparams.put("year", String.valueOf(year));
                return postparams;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(sr,TAG);
        return data;
    }



    public static Object getItineraryTurnInfo(final String shipcode, final String saildate, final VolleyCallback callback) throws JSONException {

        String url =  ServerURL+"api/ilookup/ILookUp/getItineraryTurnInfo";
        String data = null;

        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onSuccessResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFailedResponse(error);
            }
        }) {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> postparams = new HashMap<String, String>();
                postparams.put("brandcode", "ilookup");
                postparams.put("shipcode", shipcode);
                postparams.put("saildate", saildate);
                return postparams;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(sr,TAG);
        return data;
    }



    public static Object Port(final VolleyCallback callback) throws JSONException {
        String url =  ServerURL+"api/ilookup/ILookUp/getPorts";
        String data = null;
        StringRequest sr = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                callback.onSuccessResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFailedResponse(error);
            }
        }) {
        };
        MyApplication.getInstance().addToRequestQueue(sr,TAG);
        return data;
    }

    public static Object getTurnInfoByPort(final String portcode, final VolleyCallback callback) throws JSONException {

        String url =  ServerURL+"api/ilookup/ILookUp/getTurnInfoByPort?portcode="+portcode;
        String data = null;
        StringRequest sr = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onSuccessResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFailedResponse(error);
            }
        });
        MyApplication.getInstance().addToRequestQueue(sr,TAG);
        return data;
    }


}

