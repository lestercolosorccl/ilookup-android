package com.rcclpmo.a7567.ilookup;


import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "iLookUp.db";

    private HashMap hp;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//        db.execSQL("DROP TABLE login");
        db.execSQL("create table login (login_data text,  login_date date default CURRENT_DATE)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS login");
        onCreate(db);
    }

    public boolean insertLogin(JSONObject data) {

        deleteLogin();
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DROP TABLE login");
        db.execSQL("create table login (login_data text,  login_date date default CURRENT_DATE)");

        ContentValues contentValues = new ContentValues();
        contentValues.put("login_data", String.valueOf(data));
        String result = String.valueOf(db.insert("login", null, contentValues));
        return true;
    }

    public void insertPort(String data) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS port");
        db.execSQL("create table port (port_data text)");

        ContentValues contentValues = new ContentValues();
        contentValues.put("port_data", data);
        db.insert("port", null, contentValues);

//        int i;
//        for(i=0; i<data.length(); i++){
//            JSONObject portObject = null;
//            try {
//                portObject = new JSONObject(data.getString(i));
//
//                ContentValues contentValues = new ContentValues();
//                contentValues.put("port_position", i);
//                contentValues.put("portcode", portObject.getString("portcode"));
//                contentValues.put("portname", portObject.getString("portname"));
//                String result = String.valueOf(db.insert("port", null, contentValues));
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
    }

    public String getPortData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from port", null );
        res.moveToFirst();
        String data = res.getString(0);
        return data;
    }

    public JSONObject getPortByPos(Integer pos){
        JSONObject result = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from port where port_position="+pos, null );
        res.moveToFirst();

        try {
            result.put("portcode", res.getString(1));
            result.put("portname", res.getString(2));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Cursor getPorts(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select portname from port", null );
        return res;
    }

    public String getLogin() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from login", null );
        res.moveToFirst();
        String data = res.getString(0);
        return data;
    }

    public boolean checkLogin(){
        boolean result = false;
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, "login");
        if(numRows>=1){
            result = true;
        }
        return result;
    }

    public boolean updateContact (Integer id, String name, String phone, String email, String street,String place) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("street", street);
        contentValues.put("place", place);
        db.update("login", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleteLogin () {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("login", null, null);
    }

    public ArrayList<String> getUserData() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from login", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex("login")));
            res.moveToNext();
        }
        return array_list;
    }


}